export const APP_SIDEBAR_LOCATIONS = [
  {
    name: 'Sidebar.Users',
    key: 'Sidebar.Users',
    url: '/users',
    icon: 'fas fa-user',
    index: 0
  },
  {
    name: 'Sidebar.Groups',
    key: 'Sidebar.Groups',
    url: '/groups',
    icon: 'fas fa-user-friends',
    index: 0
  },
  {
    name: 'Sidebar.Permissions',
    key: 'Sidebar.Permissions',
    url: '/privileges',
    icon: 'fas fa-lock',
    index: 0
  }
];
